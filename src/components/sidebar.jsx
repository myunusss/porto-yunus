import React, { Component } from 'react'

export default class Sidebar extends Component {
  render() {
    return (
      <div>
        <div>
          <aside id="colorlib-aside" className="border js-fullheight">
            <div className="text-center">
              <div className="about-desc">
                <span className="heading-meta">-  ,  -</span>
              </div>
              <div className="author-img" style={{backgroundImage: 'url(https://storage.googleapis.com/gcs_yunus_bucket/Clodeo_Portrait_Yunus.jpg)'}} />
              <h1 id="colorlib-logo"><a href="index.html">Muhammad Yunus Turmudi</a></h1>
              <span className="email"><i className="icon-mail"></i> yunus8889@gmail.com</span>
            </div>
            <nav id="colorlib-main-menu">
              <ul>
                <li></li>
                <li><a href="https://www.instagram.com/myturmudi/" target="_blank" rel="noopener noreferrer"><i className="icon-instagram" /></a></li>
                <li><a href="https://www.linkedin.com/in/muhammad-yunus-turmudi-b855a6153/" target="_blank" rel="noopener noreferrer"><i className="icon-linkedin2" /></a></li>
                <li><a href="https://github.com" target="_blank" rel="noopener noreferrer"><i className="icon-github"></i></a></li>
              </ul>
            </nav>
            <div className="colorlib-footer">
              <p><small>
                Dimulai dengan bismillah
              </small></p>
            </div>
          </aside>
        </div>
      </div>
    )
  }
}
